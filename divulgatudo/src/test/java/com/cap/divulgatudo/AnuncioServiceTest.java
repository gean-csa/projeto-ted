package com.cap.divulgatudo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.cap.divulgatudo.model.Anuncio;
import com.cap.divulgatudo.repository.AnuncioRepository;
import com.cap.divulgatudo.service.AnuncioService;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
@WebAppConfiguration
//anotações que fazem subir o sprig quando o teste é executado
public class AnuncioServiceTest {
	
	@Autowired
	private AnuncioService anuncioService;
	
	@Autowired
	private AnuncioRepository anuncioRepository;
	
	
	@Test
	public void buscarPorId() {
		Anuncio anuncio = anuncioService.get(22L); 
		assertEquals("hotdog", anuncio.getCliente());
		assertEquals("hot", anuncio.getNome_Anuncio());
		
		Anuncio anuncio1 = anuncioService.get(23L); 
		assertEquals("oiii", anuncio1.getCliente());
		assertEquals("oi", anuncio1.getNome_Anuncio());
		
	}
/*	
	@Test
	public void deletarPorIdTest() {
		anuncioService.delete(17L);
		Optional<Anuncio> optionalAnuncio = anuncioRepository.findById(17L);
		assertFalse(optionalAnuncio.isPresent());
		
	}
*/
}
